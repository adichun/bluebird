// main
package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(1000)
	wait := make(chan bool)
	input := make(chan int)
	output := make(map[int]chan int)

	numClients := 10

	//initialize clients chan:
	for i := 0; i < numClients; i++ {
		output[i] = make(chan int)
	}

	//readers:
	for i := 0; i < numClients; i++ {
		go func(id int) {
			for {
				select {
				case res, ok := <-output[id]:
					if ok {
						clientHandler(id, res)
					}
				}
			}
		}(i)
	}

	//distributor:
	go func() {
		for {
			//wait for data from input channel:
			//variable wait interval controlled by publisher
			select {
			case res, ok := <-input:
				if ok {
					//distribute this data to output channels:
					for _, v := range output {
						v <- res
					}
				}
			}

		}
	}()

	//start input writer goroutine
	go publisher(input)

	<-wait
}

func clientHandler(id int, val int) {
	//this simulates the total delay in the client side

	k := 300
	r := k + rand.Intn(250)

	time.Sleep(time.Millisecond * time.Duration(r))

	fmt.Printf("client %d processsing event %d in %d msec\n", id+1, val, r)
}

func publisher(ch chan int) {
	// numIter := 6
	i := 0
	for {
		r := 250 + rand.Intn(500)

		time.Sleep(time.Millisecond * time.Duration(r))

		fmt.Println("publisher event...", i)

		ch <- i + 1

		// if i >= numIter {
		// 	break
		// }

		i++
	}

	fmt.Println("publisher completed....")
}
