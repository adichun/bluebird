package main

import (
	"log"
)

//BinaryTree models binary tree data structure
type BinaryTree struct {
	// please add fields
}

//Insert inserts value to the binary tree
func (tree *BinaryTree) Insert(value int) {

}

//Exist returns true if value exist in binary tree, false otherwise
func (tree *BinaryTree) Exist(value int) bool {
	return false
}

//PrintOrder prints binary tree value in correct order
func (tree *BinaryTree) PrintOrder() {

}

func main() {
	array := []int{5, 3, 6, 2, 9, 4, 7, 8, 1}
	tree := BinaryTree{}
	for _, value := range array {
		tree.Insert(value)
	}

	log.Print(tree.Exist(7))  // true
	log.Print(tree.Exist(10)) //false

	tree.PrintOrder() //will print "123456789"
}
