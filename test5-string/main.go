package main

import "fmt"

func main() {
	var a string = "pagi"
	var b string = "selamat"

	//swap value of variable a and b without using temporary variable
	//so it will print correct sentence
	b, a = a, b // answer

	fmt.Println(a, " ", b) //Print "selamat pagi"
	fmt.Println("========")
	fmt.Println(a, " ", b)
}
