package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

type mydata struct {
	list map[string]int
}

func main() {
	var md mydata
	wait := make(chan bool)
	md.list = make(map[string]int)

	//launch 10 goroutines
	//each will perform read/write ops
	//
	for j := 0; j < 10; j++ {
		go func(val int) {
			for i := val; i < val+1000; i++ {
				md.mapWrite("key"+strconv.Itoa(i), i)
			}
		}(j * 1000)
	}

	<-wait
}

//create a random delay:
func getRandomDelay(delay int) time.Duration {

	return time.Millisecond * time.Duration(delay+rand.Intn(50))
}

func (m *mydata) mapWrite(key string, newval int) {
	time.Sleep(getRandomDelay(60))

	val, ok := m.list[key]
	if ok { // karena oke return nya false, true
		m.list[key] = val
		val = newval
	}

	fmt.Printf("writing map data for key: %s val: %d error: (%v)\n", key, val, ok)
}
