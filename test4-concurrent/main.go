// main
package main

import (
	"fmt"
	"math/rand"

	"sync"
	"time"
)

func main() {
	rand.Seed(1000)

	input := make(chan int)
	output := make(map[int]chan int)

	numClients := 10
	var wg sync.WaitGroup

	//initialize clients chan:
	for i := 0; i < numClients; i++ {
		output[i] = make(chan int)
	}

	//readers:
	for i := 0; i < numClients; i++ {
		go func(id int) {
			//get the channel corresponding to the param id:
			myChan := output[id]
			for {
				if res, ok := <-myChan; ok {
					clientHandler(id, res)
				} else {
					fmt.Println(">>>>>>>>>>>>>>>>> exit reader goroutines ", id)
					break
				}
			}

		}(i)
	}

	//distributor:
	go func() {

		for {
			//wait for data from input channel:
			//variable wait interval controlled by publisher
			select {
			case res, ok := <-input:
				if ok {
					//distribute this data to output channels:
					for _, v := range output {
						v <- res
					}
				} else {
					fmt.Println(">>>>>>>>>>>>>>> exit publisher goroutine")
					for _, v := range output {
						close(v)
					}

					return
				}
			}
		}
	}()

	//start input writer goroutine
	go publisher(input)
	wg.Wait()

	fmt.Println("<<<<<<<<<<<<<<<<< Exit App")
}

func clientHandler(id int, val int) {
	//this simulates the total delay in the client side

	k := 300
	r := k + rand.Intn(250)

	time.Sleep(time.Millisecond * time.Duration(r))

	fmt.Printf("event %d received by reader %d in %d msec\n", val, id+1, r)
}

func publisher(ch chan int) {

	numIter := 20

	for i := 0; i < numIter; i++ {
		r := 250 + rand.Intn(500)

		time.Sleep(time.Millisecond * time.Duration(r))

		fmt.Println("publishing event...", i)

		ch <- i + 1
	}

	fmt.Println("publisher completed")
}
