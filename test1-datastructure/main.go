// main
package main

import (
	"fmt"
	"strconv"
)

func sumString() string {
	var a int
	var s string

	a = 1
	s = "abc"
	b := strconv.Itoa(a)
	res := b + s
	return res
}

func sumInt() int {
	var a int
	var s string

	a = 1
	s = "abs"
	b, _ := strconv.Atoi(s)
	res := a + b
	return res
}

type SumStruct struct {
	A int
	S string
}

func UseStruct(A int, S string) *SumStruct {
	number := SumStruct{A: A, S: S}
	return &number
}

func main() {
	var sumString = sumString()
	var sumInt = sumInt()

	fmt.Println("result", sumString, sumInt)
	fmt.Println("result just struct", SumStruct{10, "abc"})
	fmt.Println("result using struct", UseStruct(100, "abc"))
}
